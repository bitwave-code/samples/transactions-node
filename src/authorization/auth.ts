import {OauthResponse} from "./oauthResponse";
import {Oauth} from "./oauth";

export class Auth {
    static async getToken(
        baseUrl: string,
        clientId: string,
        clientSecret: string
    ): Promise<OauthResponse>{
        const oauth: Oauth = new Oauth(baseUrl, clientId, clientSecret);
        const token: OauthResponse = await oauth.getToken();
        return token;
    };

}