import {OauthResponse} from "./oauthResponse";
import axios, {AxiosResponse} from "axios";

export class Oauth {
  route = '/oauth';
  clientId: string;
  clientSecret: string;
  baseUrl: string;

  constructor(baseUrl: string, clientId: string, clientSecret: string) {
    this.baseUrl = baseUrl;
    this.clientId = clientId;
    this.clientSecret = clientSecret;
  }

  public async getToken(): Promise<OauthResponse> {
    const url = `${this.baseUrl}${this.route}/token`;
    const data = {
      grant_type: 'client_credentials',
      client_id: this.clientId,
      client_secret: this.clientSecret,
    };
    const resp: AxiosResponse<OauthResponse> = await axios.post(
      url,
      data
    );
    return resp.data;
  }
}
