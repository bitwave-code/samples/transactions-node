export interface OauthResponse {
  access_token: string;
  token_type: 'Bearer';
  expires_in: number;
}
