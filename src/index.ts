import {Import} from './import';

Import.run()
  .then(() => {
    console.log('Completed Import');
  })
  .catch(e => {
    console.log('Import failed', e);
  });
