import {SimpleLogger} from '@bitwave-code/common';
import {TransactionServiceGatewayImpl} from '@bitwave-code/transactions/dist/src';
import axios from 'axios';
import {
  Sources,
  TradeTransaction,
  TradeType,
  TxnCryptoAmountTypeEnum,
  TxnFiatAmountTypeEnum,
} from '@bitwave-code/transactions/dist/generated/transaction-svc';
import {Auth} from "./authorization/auth";

export class Import {
  static async run() {
    const apiKey = 'YOUR_KEY';
    const apiSecret = 'YOU_SECRET';
    const authResp = await Auth.getToken("https://api.bitwave.io", apiKey, apiSecret);

    const transactionSvc = new TransactionServiceGatewayImpl(
      new SimpleLogger(),
        authResp.access_token,
      'https://api.bitwave.io',
      axios.create(),
      true
    );

    const t: TradeTransaction = {
      type: 'trade',
      tradeType: TradeType.Buy,
      source: Sources.Custom,
      remoteSystemId: {
        remoteId: 'your Id from your system',
        source: Sources.Custom
      },
      acquiredAssets: [
        {
          amount: "100",
          ticker: 'BTC',
          type: TxnCryptoAmountTypeEnum.Crypto
        }
      ],
      disposedAssets: [
        {
          amount: "430000",
          ticker: 'USD',
          type: TxnFiatAmountTypeEnum.Fiat
        }
      ],
      isComplete: true,
      createdSEC: 123, //Time the transaction was created in unix epoch time
      walletId: 'YOUR MANUAL WALLET ID',
      walletName: 'NA', // this is deprecated
      fees: []
    };
    //
    // // You can use anything as sourceId here - if you push two transactions with
    // // the same transactionId, but different source ids, you'll actually create
    // // two embedded transactions, and the transaction will represent the sum of
    // // the two transactions.
    // // If you push with the same sourceId, the second push will overwrite the first
    // const sourceId = '100';
    //
    await transactionSvc.upsert(
      {traceId: 'unique trace id '}, // traceid helps with debugging + logging
      'your org ID',
      'your wallet ID',
      'SOURCE', // This is important, read our documentation about it
      Sources.Custom,
      t
    );
  }
}
